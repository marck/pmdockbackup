#!/usr/bin/env bash

RED='\033[0;31m'
YEL='\033[1;33m'
BLU='\033[1;34m'
NC='\033[0m' # No Color

i=1
echo -e "${BLU}[+] Starting Backup for multiple Stacks${NC}"
for file in "$PWD"/*/backup/backup.sh
do
  cd "$(dirname "$(dirname "$file")")" || exit
  ((i++))
  printf '=%.0s' {1..80}
  echo -e "\n${YEL}[-] Entering $PWD${NC}\n"
  printf '=%.0s' {1..80}
  echo -e "\n"
  backup/backup.sh
  echo -e "\n[+] Leaving $PWD"
done

printf '=%.0s' {1..80}
echo -e "\n${YEL}[+] Backuped $i Stacks${NC}"
echo -e "${RED}[+] Unsetting env. variables (again) for the flies${NC}"
printf '=%.0s' {1..80}
echo -e "\n"

unset RESTIC_REPOSITORY
unset RESTIC_PASSWORD
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
