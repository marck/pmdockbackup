#!/usr/bin/env bash

#MAILTO="example.email@gmail.com"
#CRON_SCHEDULE=0 1 * * * # every day
# 2>&1
RED='\033[0;31m'
GRE='\033[1;32m'
YEL='\033[1;33m'
BLU='\033[1;34m'
NC='\033[0m' # No Color


echo "[+] Backup started"

# Relative or absolute path
TO_BACKUP="$PWD"

echo -e "${YEL}[+] Backuping $TO_BACKUP${NC}"

# Use docker-compose environment file to import variables
source ".env"

. backup/load_vars.sh

#TODO: make a loop to stop multiple DB containers


#docker exec db /usr/bin/mysqldump -u root --password=supersecretpassword --all-databases > dump/`date +%Y-%m-%d-%T%z`-backup.sql
#docker exec db /usr/bin/mysqldump -u root --all-databases > $COMPOSE_PROJECT_NAME-`date +%Y-%m-%d-%T%z`-backup.sql

echo -e "[+] Dumping all databases"

mkdir -p volumes/dumps

if [ -n "$MYSQL_HOST" ]
then
	echo -e "  ${GRE}[-] Found a MySQL or MariaBD database${NC}"
	docker-compose exec $MYSQL_HOST /usr/bin/mysqldump \
		--lock-all-tables \
		-u root \
		--password=$MYSQL_ROOT_PASSWORD \
		--all-databases \
		| gzip \
		> volumes/dumps/$COMPOSE_PROJECT_NAME-$(date +%F_%H%M%S)-backup.sql.gz
elif [ -n "$POSTGRES_USER" ]
then
	echo -e "  [-] ${GRE}Found a PostGRESQL database${NC}"
	docker-compose exec -u $POSTGRES_USER db pg_dumpall -c  | gzip > volumes/dumps/$COMPOSE_PROJECT_NAME-$(date +%F_%H%M%S)-backup.sql.gz
else
	echo -e "${RED}No database container?${NC}"
fi

echo "[+] Stoping containers"
# Stoping containers with a DB to maintaint DB coherence
docker-compose stop db
docker-compose pause

restic backup $TO_BACKUP --exclude-file=backup/excludes.txt --tag $COMPOSE_PROJECT_NAME

echo -e "${GRE}[+] Re-starting Containers${NC}"

docker-compose unpause
docker-compose up -d

echo "[+] Unseting env. variables"

unset RESTIC_REPOSITORY
unset RESTIC_PASSWORD
unset AWS_ACCESS_KEY_ID
unset AWS_SECRET_ACCESS_KEY
